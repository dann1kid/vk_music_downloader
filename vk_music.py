import vk_api
from vk_api import audio
import requests
import time
import random
from slugify import slugify
# мультипоток 
from multiprocessing.dummy import Pool as ThreadPool
# определить количество процессоров и выделить N + 1
from psutil import cpu_count
# полоса загрузки
from progress.spinner import LineSpinner
# сохр предыдущую сессию
import shelve

# добавить путь
# добавить requirements
# TODO	прикрутить загрузку альбомов
# TODO	прикрутить загрузку песен и альбомов с других страниц


def get_all_tracks(vk_audio):
	tracks = []
	audio = vk_audio.get()
	spinner = LineSpinner(message=".")
	spinner.next()
	for track in audio:
		tracks.append(track)
		spinner.next()
	return tracks


def write_track(track):
	random_n = random.randint(0, 999)
	random_w = random.choice("abcdefghijklmopqrstuvwxyz")
	track_name = track['title']
	# создание универсального имени для защиты от ошибок файловой системы и одинаковых по названию треков
	track_name = f"{slugify(track_name)}_id_{str(random_n)}_{random_w}"
	spinner = LineSpinner(message="В работе...")
	spinner.next()
	
	with open(f"{track_name}.mp3", "wb") as file:
		response = requests.get(track['url'])
		try:
			for chunk in response.iter_content(1 << 12):
				file.write(chunk)
				spinner.next()
		except OSError:
			print("Ошибка записи.")


def main():
	
	# user input
	login = input("Введи логин (если не введешь, будут использованы старые данные): \n")
	
	if login == '':
		try:
			with shelve.open('session') as db:
				login = db['login']
				password = db['password']
		
		except KeyError:
			print("Не введены данные и не найдено сохраненных.")
	
	else:
		with shelve.open('session') as db:
			db['login'] = login
		
		password = input("Введи пароль: \n")
		with shelve.open('session') as db:
			db['password'] = password
	
	vk = vk_api.VkApi(login=login, password=password)
	
	# auth
	print("Пытаюсь авторизоватся...")
	vk.auth()
	vk_audio = audio.VkAudio(vk)
	print("Успешно!")
	
	# download
	print('Получаю список треков... ')
	
	list_tracks = get_all_tracks(vk_audio)
	
	print("Количество песен - ", len(list_tracks))
	time.sleep(1)
	
	print("Приступаю к загрузке...")
	
	# количество тредов
	pool = ThreadPool(cpu_count() + 1)
	# результат
	downloader = pool.map(write_track, list_tracks)
	
	pool.close()
	pool.join()


if __name__ == '__main__':
	main()
